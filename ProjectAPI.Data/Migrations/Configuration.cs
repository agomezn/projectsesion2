namespace ProjectAPI.Data.Migrations
{
    using Model;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ProjectAPI.Data.ApiContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ProjectAPI.Data.ApiContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            Book book = new Book();
            book.Title = "Midnight Rain";
            book.Price = 14.95;
            book.Genre = "Fantasy";
            book.PublishDate = new DateTime(2000, 12, 16);
            book.Description = "A former architect battles an evil sorceress.";
            // insert into Book(dat1, dat,2dat3) Values("","","")
            context.Books.AddOrUpdate(book);
            // agregra
            //context.Books.Add(book);
            // consultar
            //context.Books.Find(book);
            //eliminar
            //context.Books.Remove(book);
        }
    }
}
