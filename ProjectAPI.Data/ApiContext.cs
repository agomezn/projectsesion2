﻿using ProjectAPI.Data.Mapping;
using ProjectAPI.Data.Model;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace ProjectAPI.Data
{
    public class ApiContext : DbContext
    {
        public ApiContext() : base("ProjectApi")
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<ApiContext>());
        }

        public DbSet<Book> Books { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Configurations.Add(new BookMapping());
        }
    }
}
